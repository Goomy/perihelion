#include "baseShape.h"
#include <SFML/Graphics.hpp>

using namespace sf;
// this class gets used for multiple different objects in the game
baseShape::baseShape(float x,float y, int sizew, int sizeh)
{
	// setting parameters of the base shape to those specified above
	sizex = sizew;
	sizey = sizeh;
	isonGround = false;
	isengineOn = false;
	setSize(Vector2f(sizew,sizeh));
	setPosition(x, y);
	colliderrect = getGlobalBounds();
	setFillColor(Color::Red);
}

baseShape::baseShape(float x,float y, int sizew, int sizeh, Texture *image)
{
	// setting parameters of the base shape to those specified above
	sizex = sizew;
	sizey = sizeh;
	isonGround = false;
	isengineOn = false;
	setSize(Vector2f(sizew,sizeh));
	setPosition(x, y);
	colliderrect = getGlobalBounds();
	setTexture(image);
}

baseShape::baseShape(float x,float y, int sizew, int sizeh, Texture *image, IntRect intrect)
{
	// setting parameters of the base shape to those specified above
	sizex = sizew;
	sizey = sizeh;
	isonGround = false;
	isengineOn = false;
	setSize(Vector2f(sizew,sizeh));
	setPosition(x, y);
	colliderrect = getGlobalBounds();
	setTexture(image);
	setTextureRect(intrect);
}

baseShape::baseShape(float x,float y, int sizew, int sizeh, Texture *image, Color col)
{
	// setting parameters of the base shape to those specified above
	sizex = sizew;
	sizey = sizeh;
	isonGround = false;
	isengineOn = false;
	setSize(Vector2f(sizew,sizeh));
	setPosition(x, y);
	colliderrect = getGlobalBounds();

	setTexture(image);
}

void baseShape::updatecol(int x, int y)
{
	setPosition((float) x,(float) y);
	colliderrect = getGlobalBounds();
}