#include "baseShape.h"
#include <list>
#include <string>
#include <cmath>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include "particle.h"
#include "rSprite.h"
#include "Fuel.h"
#include <fstream>
#include <iostream>
#include <random>
#include "res.png.h"
#include "RCS.h"
#include "missile.h"
using namespace sf;
// window size. Eventually a selectable resolution.
int res[2] = {1200, 600};
String title = "Perihelion";
// rocket entity
//players view
View camera;
View info;
View SunV;
// font and text variables;
Font font;
Text enfuelt;
Text rcsfuelt;
Vector2f rsize(60,60);
RenderWindow *windowP;
// size of ground array
Image Res;
Uint8 * rss;
const int gsize = 200;
const int maxP = 100;
int fuel = 2000;
int rcsfuel = 400;
double enginepower = 0.1;
float pang = 30;
int ALT;
int vv = 0;
double rcspower = 0.05;
float airfric = 0.1;
float fps, lasttime = 0;

sf::Clock colck;
const int staramount = 5000;
const int ftAmt = 30;
int mapSize = res[0]*4;
Fuel FuelTanks[ftAmt];
bool isdebug = false;
Texture rtext;
Texture ftext;
Texture * rtextP;
Texture * ftextP;
Texture gtext;
Texture * gtextP;
Texture rcstext;
Texture * rcstextP;
Texture SunText;
Texture * SunTextP;
Texture thrutext;
Texture * thrutextP;
Texture missText;
Texture * missTextP;
Texture Cursor;
Texture * CursorP;
Texture Cook;
RCS rcsJets[4];
particle starf[staramount];
baseShape Sun;
int plifetime = 1;
unsigned char *datas;
float shitvel = 0;
bool ismove = false;
bool isJoystick = false;
bool isFinished = false;
bool isThrustForward, isThrustBackward = false;
bool isrunning = false;
bool ispaused = true;
bool iscookie = false;
double grav = 0.05;
float angle;
Vector2i pixpos;
std::list<particle> plist (100);
std::list<missile> mlist(10);
ContextSettings settings;

Joystick joy;
//array of blocks to make up the ground
baseShape ground[gsize];
Sprite CSprite;
Mutex mute;
//prototype for random runction that returns a random number with a range
int randrange(int max, int min);
void drawScore(RenderWindow window);
void starField();
void setdebugMode();
void groundGen();
void hillgen(int x, int y);
void fgen();
void moveRight(int o);
void moveLeft(int o);
void physics();
int main()
{
	//setting up the window
	settings.antialiasingLevel = 8;
	RenderWindow window(VideoMode(res[0], res[1], 32U), title, Style::Titlebar|Style::Close,settings);
	windowP = &window;
	if(sf::Joystick::isConnected(0))
	{
		isJoystick = true;
	}
	if(!font.loadFromFile("C:/Windows/Fonts/arial.ttf"))
	{
		if(!font.loadFromFile("./arial.ttf"))
		{
			return 1;
		}
	}
	if(!Res.loadFromMemory(res_png, sizeof(res_png)))
	{
		return EXIT_FAILURE;
	}else
	{
		rtext.loadFromImage(Res, IntRect(0,0,60,60));
		rtextP = &rtext;
		ftext.loadFromImage(Res, IntRect(60,0,120,60));
		ftextP = &ftext;
		gtext.loadFromImage(Res, IntRect(0,61,60,121));
		gtext.setRepeated(true);
		gtextP = &gtext;
		rcstext.loadFromImage(Res,IntRect(60,60,120,80));
		rcstextP = &rcstext;
		SunText.loadFromImage(Res, IntRect(0,180,120,300));
		SunTextP = &SunText;
		thrutext.loadFromImage(Res, IntRect(59,159,64,164));
		thrutextP = &thrutext;
		missText.loadFromImage(Res,IntRect(60,64,100,80));
		missTextP = &missText;
		Cursor.loadFromImage(Res,IntRect(59,164,69,174));
		Cook.loadFromImage(Res,IntRect(59,174,69,184));
	}
	CSprite = Sprite(Cursor);
	CSprite.setScale(Vector2f(5,5));
	CSprite.setTextureRect(IntRect(0,0,10,10));
	CSprite.setColor(Color::Color(255,255,255,150));
	window.setMouseCursorVisible(false);
	srand((unsigned) time(0));
	camera = View(FloatRect(0,0,res[0],res[1]));
	info = View(FloatRect(0,0,200,200));
	info.setViewport(sf::FloatRect(0, 0, 0.25f, 0.25f));
	SunV = View(FloatRect(0,0,120,120));
	SunV.setViewport(sf::FloatRect(0.85f, 0, 0.15f , 0.20f));
	window.setFramerateLimit(60);
	window.setVerticalSyncEnabled(false);
	enfuelt.setFont(font);
	rcsfuelt.setFont(font);
	enfuelt.setCharacterSize(20);
	rcsfuelt.setCharacterSize(20);
	enfuelt.setStyle(Text::Bold);
	enfuelt.setPosition(10,10);
	rcsfuelt.setPosition(10, 30);
	enfuelt.setColor(Color::Red);
	rcsfuelt.setColor(Color::Red);

	//initializing rocket(boosters lol)
	rSprite rocket = rSprite(30, res[1]-60, rtextP);
	rocket.isDamaged = false;
	rcsJets[0] = RCS(rocket.getPosition().x+59,rocket.getPosition().y-10, 0);
	rcsJets[1] = RCS(rocket.getPosition().x+59,rocket.getPosition().y-10,1);
	rcsJets[2] = RCS(rocket.getPosition().x,rocket.getPosition().y+16,0);
	rcsJets[3] = RCS(rocket.getPosition().x,rocket.getPosition().y+36, 0);
	Sun = baseShape(0,0,120,120,SunTextP);
	//setting up the ground
	groundGen();
	starField();
	fgen();
	//main loop
	isrunning = true;
	ispaused = false;
	while(window.isOpen())
	{
		//event var followed by the event loop for handling input
		//std::cout << Joystick::getAxisPosition(0, Joystick::Axis::X)/100 << std::endl;
		Event eve;
		while(window.pollEvent(eve))
		{
			//handling all events
			switch(eve.type)
			{
			case Event::Closed:

				isrunning = false;
				window.close();
				break;

			case Event::JoystickButtonPressed:
				//std::cout << eve.joystickButton.button << std::endl;
				break;
			case Event::KeyPressed:

				if(Keyboard::isKeyPressed(Keyboard::Escape))
				{
					if(ispaused)
					{
						ispaused = false;
					}else
					{
						ispaused = true;
					}
				}
				if(Keyboard::isKeyPressed(Keyboard::F12))
				{
					rocket.isDamaged = false;
					setdebugMode();
				}
				break;
			case Event::KeyReleased:
				switch(eve.key.code)
				{
				case Keyboard::Space:
					rocket.isengineOn = false;
					plifetime = 1;
					break;
				case Keyboard::D:
					airfric = 0.1;
					isThrustForward = false;
					ismove = false;
					break;
				case Keyboard::A:
					airfric = 0.1;
					isThrustBackward = false;
					ismove = false;
					break;
				}

				break;
			case Event::MouseWheelMoved:
				switch(eve.mouseWheel.delta)
				{
				case 1:
					camera.zoom(1);
					break;
				case -1:
					camera.zoom(-1);
					break;
				}
				break;
			case Event::MouseButtonPressed:
				if(Mouse::isButtonPressed(Mouse::Left))
				{
					Vector2f start = rocket.getPosition() + Vector2f(30,30);
					pixpos = Mouse::getPosition(window);
					Vector2f targ = window.mapPixelToCoords(pixpos);
					Vector2f delta = targ - start;

					angle = atan2f(delta.y, delta.x) * 180 / PI;
					mute.lock();
					if(mlist.size() < 10)
					{
						missile mtmp(Vector2f(camera.getCenter().x,rocket.getPosition().y),missTextP,angle);
						mlist.push_front(mtmp);
					}
					mute.unlock();

				}
				break;

			case Event::JoystickButtonReleased:
				switch(eve.joystickButton.button)
				{
				case 0:
					rocket.isengineOn = false;
					break;
				}
				break;
			case Event::JoystickMoved:
				if(isJoystick)
				{
					if(Joystick::getAxisPosition(0, Joystick::Axis::X) > 99)
					{
						moveRight(1);
					}else{
						airfric = 0.1;
						isThrustForward = false;
						ismove = false;
					}
					if(Joystick::getAxisPosition(0, Joystick::Axis::X) < -98)
					{
						moveLeft(1);
					}else{
						airfric = 0.1;
						isThrustBackward = false;
						ismove = false;
					}
				}
			}
		} 
		ALT = (int) ground[0].getPosition().y-rocket.getPosition().y-60;
		float thistime = colck.restart().asSeconds();
		fps = 1.f / thistime;
		lasttime = thistime;

		if(Keyboard::isKeyPressed(Keyboard::D))
		{
			if(rocket.getPosition().x+60 < mapSize)
			{
				moveRight(0);
			}
			else{
				shitvel = 0;
			}
		}
		if(Keyboard::isKeyPressed(Keyboard::A))
		{
			if(rocket.getPosition().x > 29)
			{
				moveLeft(0);
			}
			else{
				shitvel = 0;
			}
		}
		/*if(Keyboard::isKeyPressed(Keyboard::Up))
		{
		rocket.rotate(1);
		}
		if(Keyboard::isKeyPressed(Keyboard::Down))
		{
		rocket.rotate(-1);
		}*/
		if(rocket.dy < 0)
		{
			vv = abs(rocket.dy);
		}else
		{
			vv = -rocket.dy;
		}
		// ground collision detection
		for(int j = 0;j<gsize;j++)
		{
			if(rocket.colliderrect.intersects(ground[j].colliderrect))
			{
				if(rocket.dy > 5)
				{
					rocket.isDamaged = true;
					rocket.isengineOn = false;
				}
				rocket.dy = 0;
				shitvel *= 0.5;
				rocket.isonGround = true;
				if((rocket.getPosition().y+60)>ground[j].getPosition().y)
				{
					rocket.updatecol(rocket.getPosition().x, ground[j].getPosition().y-60, rtextP);
				}
			}else{
				rocket.isonGround = false;
			}
		}

		for(int i = 0;i<ftAmt;i++)
		{
			if(rocket.colliderrect.intersects(FuelTanks[i].colliderrect))
			{
				if(fuel < 2000)
				{
					if(!FuelTanks[i].pickedup)
					{
						fuel += FuelTanks[i].fuelamount;
						if(fuel > 2000)
						{
							fuel = 2000;
						}
						FuelTanks[i].setColor(Color(0,0,0,0));
						FuelTanks[i].pickedup = true;
					}
				}
			}
		}
		if(!ispaused)
		{
			if(!rocket.isonGround)
			{
				rocket.dy += grav;
			}
			if(rocket.dy > 30)
			{
				rocket.dy = 30;
			}
		}
		if(Keyboard::isKeyPressed(Keyboard::LShift) && Keyboard::isKeyPressed(Keyboard::BackSpace))
		{
			if(!iscookie)
			{
				iscookie = true;
				CSprite.setTexture(Cook);
				CSprite.setTextureRect(IntRect(0,0,10,10));
			}else
			{
				iscookie = false;
				CSprite.setTexture(Cursor);
				CSprite.setTextureRect(IntRect(0,0,10,10));
			}
		}
		if(Keyboard::isKeyPressed(Keyboard::Space) || Joystick::isButtonPressed(0,0))
		{
			if(!rocket.isDamaged)
			{
				if(fuel > 0)
				{
					fuel -= enginepower;
					if(rocket.dy > -10)
					{
						rocket.dy -= enginepower;
					}
					if(plifetime < 120)
					{
						plifetime += 1;
					}
					rocket.isengineOn = true;
					rocket.isonGround = false;
				}else{
					fuel = 0;
				}
			}
		}

		//draws the rocket
		rocket.move(shitvel, rocket.dy);
		rocket.colliderrect = rocket.getGlobalBounds();
		rcsJets[0].update(rocket.getPosition().x+59,rocket.getPosition().y+22,1);
		rcsJets[1].update(rocket.getPosition().x+59,rocket.getPosition().y+42,1);
		rcsJets[2].update(rocket.getPosition().x,rocket.getPosition().y+22,0);
		rcsJets[3].update(rocket.getPosition().x,rocket.getPosition().y+42,0);
		//std::cout << rocket.getPosition().x << "  " << rocket.getPosition().y << std::endl;

		window.clear(Color(0,0,0,255));
		window.setView(camera);

		//sets background color


		if(rocket.getPosition().x < 0)
		{
			//
		}
		camera.setCenter(rocket.getPosition().x+30, rocket.getPosition().y-100);

		for(int j = 0;j<staramount;j++)
		{
			if(camera.getViewport().contains(starf[j].collrect.getPoint(0)))
			{
				window.draw(starf[j]);
			}
		}
		window.setView(camera);
		pixpos = Mouse::getPosition(window);
		CSprite.setPosition((Vector2f) window.mapPixelToCoords(pixpos - Vector2i(25,25)));

		if(plist.size() < maxP)
		{
			particle temp(Vector2f(rocket.getPosition().x+19, rocket.getPosition().y+50),grav, thrutextP,plifetime);
			plist.push_back(temp);
		}
		for(std::list<particle>::iterator it = plist.begin(); it !=plist.end();++it)
		{
			if(it->lifetime < 1 || it->getPosition().y-rocket.getPosition().y > 95)
			{
				it = plist.erase(it);
			}else
			{
				it->update(rocket.getPosition().y);
				if(rocket.isengineOn)
				{
					window.draw(*it);
				}
			}
		}
		for(std::list<missile>::iterator it = mlist.begin(); it !=mlist.end();++it)
		{
			if(it->lifetime < 0)
			{
				it = mlist.erase(it);
			}
			else
			{
				it->update();
				windowP->draw(*it);
			}
		}


		//draws the ground
		for(int g = 0;g<gsize;g++)
		{
			if(!camera.getCenter().y+res[1]/2 < ground[g].getPosition().y)
			{
				if(camera.getViewport().contains(ground[g].getPoint(0)))
				{
					window.draw(ground[g]);
				}
			}
		}
		for(int i = 0;i<ftAmt;i++)
		{
			window.draw(FuelTanks[i] );
		}

		window.draw(rocket);
		if(isThrustForward)
		{
			window.draw(rcsJets[2].va);
			window.draw(rcsJets[3].va);
			for(int i = 0; i < 30;i++)
			{
				window.draw(rcsJets[2].va2);
				window.draw(rcsJets[3].va2);
			}
		}
		if(isThrustBackward)
		{
			window.draw(rcsJets[0].va);
			window.draw(rcsJets[1].va);
		}
		rcsfuelt.setPosition(10, 30);
		enfuelt.setString("Fuel: "+std::to_string(fuel));
		rcsfuelt.setString("RCS: "+std::to_string(rcsfuel));
		window.setView(info);
		window.draw(enfuelt);
		window.draw(rcsfuelt);
		rcsfuelt.setPosition(20, 50);
		rcsfuelt.setString("ALT: "+std::to_string(ALT));
		window.draw(rcsfuelt);
		rcsfuelt.setPosition(10, 70);
		rcsfuelt.setString("Vertical Velocity: "+std::to_string(vv));
		window.draw(rcsfuelt);
		rcsfuelt.setPosition(10, 90);
		rcsfuelt.setString("FPS: "+std::to_string(fps));
		window.draw(rcsfuelt);
		window.setView(camera);
		//updates the window surface
		window.draw(CSprite);
		window.display();
	}
	return 0;
}
// that randome range thing from earlier
int randrange(int max, int min)
{
	return rand() % (max - min + 1)+min;
}

void drawScore(RenderWindow window)
{
}

void starField()
{
	for(int i = 0;i<staramount;i++)
	{
		int s = randrange(6, 1);
		int trans = randrange(255, 100);
		switch(randrange(3,0))
		{
		case 0:
			starf[i] = particle(Vector2f(randrange(res[0]*8, -1000), randrange(res[1]+1000, -1000*4)),thrutextP,Color(255,255,255,trans));
			break;
		case 1:
			starf[i] = particle(Vector2f(randrange(res[0]*8, -1000), randrange(res[1]+1000, -1000*4)),thrutextP,Color(253,255,214,trans));
			break;
		case 2:
			starf[i] = particle(Vector2f(randrange(res[0]*8, -1000), randrange(res[1]+1000, -1000*4)),thrutextP,Color(255,225,225,trans));
			break;
		case 3:
			starf[i] = particle(Vector2f(randrange(res[0]*8, -1000), randrange(res[1]+1000, -1000*4)),thrutextP,Color(220,252,255,trans));
			break;
		}
	}
}

void setdebugMode()
{
	if(!isdebug)
	{
		isdebug = true;
		fuel = 100000;
		rcsfuel = 100000;
	}else
	{
		isdebug = false;
		fuel = 1000;
		rcsfuel = 1000;
	}
}

void groundGen()
{
	for(int i = 0;i<gsize;i++)
	{
		ground[i] = baseShape(-1000+i*300,res[1], 300,1000,gtextP);
	}
	//ground = baseShape::baseShape(-1000,res[1], 2000*30,400,gtextP);
	//ground.setFillColor(Color::Color(182,182,182,255));
}

void hillgen(int x, int y)
{
	//nothing yet
}

void fgen()
{
	for(int i = 1;i < ftAmt; i++)
	{
		FuelTanks[i] = Fuel(i*400, ground[0].getPosition().y-60, ftextP);
	}
}

void moveRight(int o)
{
	switch(o)
	{
	case 0:
		if(rcsfuel > 0)
		{
			ismove = true;
			airfric = 0;
			rcsfuel -= rcspower;
			isThrustForward = true;

			if(shitvel < 10)
			{
				shitvel += rcspower;
			}
		}
		break;
	case 1:
		if(rcsfuel > 0)
		{
			ismove = true;
			airfric = 0;
			rcsfuel -= rcspower;
			isThrustForward = true;

			if(shitvel < 10)
			{
				shitvel += rcspower*2;
			}
		}
	}
}

void moveLeft(int o)
{
	switch(o)
	{
	case 0:
		if(rcsfuel > 0)
		{
			ismove = true;
			airfric = 0;
			rcsfuel -= rcspower;
			isThrustBackward = true;
			if(shitvel > -10)
			{
				shitvel -= rcspower;
			}
		}
		break;
	case 1:
		if(rcsfuel > 0)
		{
			ismove = true;
			airfric = 0;
			rcsfuel -= rcspower;
			isThrustBackward = true;
			if(shitvel > -10)
			{
				shitvel -= rcspower*2;
			}
		}
	}
}

/*void physics()
{
	while(isrunning)
	{
		sleep(milliseconds(60));
		mute.lock();
		std::cout << "threading lightley" << std::endl;
		
		mute.unlock();
	}

}*/