#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>
using namespace sf;
class baseShape: public RectangleShape
{
private:
	int resol[2];
	int range1;
public:
	//size of rectangle
	int sizex, sizey;
	// these will be used for gravity and velocity later
	float xx,yy,dx, dy;
	// this is for basic flight functionality
	bool isonGround;
	// bool for the engine being on
	bool isengineOn;
	// rectangle for collision detection
	FloatRect colliderrect;
	baseShape(float x,float y, int sizew, int sizeh);
	baseShape(float x,float y, int sizew, int sizeh, Texture *image);
	baseShape(float x,float y, int sizew, int sizeh, Texture *image, IntRect intrect);
	baseShape(float x,float y, int sizew, int sizeh, Texture *image, Color col);
	baseShape() {};
	void updatecol(int x, int y);
	void updatecol(){
		colliderrect = getGlobalBounds();
	};
};
