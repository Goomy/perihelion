#include "rSprite.h"
#include <SFML/Graphics.hpp>

using namespace sf;
// this class gets used for multiple different objects in the game
rSprite::rSprite(float x,float y, Texture *image)
{
	// setting parameters of the base shape to those specified above
	setTexture(*image);
	Texture rtext = *image;
	rato = 0;
	rtext.setSmooth(true);
	isonGround = false;
	isengineOn = false;
	isDamaged = false;
	setPosition((float) x, (float) y);
	colliderrect = FloatRect(x, y, x+60, y+60);
}
void rSprite::updatecol(int x, int y,Texture *image)
{
	setPosition((float) x,(float) y);
	colliderrect = getGlobalBounds();

	if(isDamaged)
	{
		setColor(Color(255,0,0,240));
	}
	else
	{
		setColor(Color(255,255,255,255));
	}
}