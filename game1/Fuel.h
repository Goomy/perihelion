#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>
class Fuel: public sf::Sprite
{
public:
	//size of rectangle
	int sizex, sizey;
	int fuelamount;
	float xx,yy,dx, dy;
	sf::Texture ftext;
	bool pickedup;
	// rectangle for collision detection
	sf::FloatRect colliderrect;
	Fuel(float x, float y, sf::Texture *image);
	Fuel(){};
	~Fuel(void);
	void updatecol(){
		colliderrect = getGlobalBounds();
	};
};
