#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>
using namespace sf;
class rSprite: public Sprite
{
private:
	int resol[2];
	int range1;
public:
	//size of rectangle
	int sizex, sizey;
	// these will be used for gravity and velocity later
	float xx,yy,dx, dy;
	// this is for basic flight functionality
	bool isonGround;
	Texture rtext;
	// bool for the engine being on
	bool isengineOn;
	bool isDamaged;
	float rato;
	// rectangle for collision detection
	FloatRect colliderrect;
	rSprite(float x,float y, Texture *image);
	rSprite(void);
	void updatecol(int x, int y,Texture *image);
	void updatecol(){
		colliderrect = getGlobalBounds();
	};
};
