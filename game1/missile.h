#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <cmath>
#define PI 3.14159265

class missile: public sf::Sprite
{
public:
	sf::Vector2f Velocity;
	float speed;
	double grav;
	int lifetime;
	float Agnle;
	missile(sf::Vector2f origin, sf::Texture *image,float angle);
	void update();
	missile(void);
	~missile(void);
};

