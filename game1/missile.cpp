#include "missile.h"

#include <iostream>


using namespace sf;
missile::missile(void)
{
}

missile::missile(Vector2f origin, Texture *image, float angle)
{
	setPosition(origin);
	grav = 0.05;
	setTexture(*image);
	Agnle = angle;
	setTextureRect(IntRect(0,0,40,16));
	speed = 10;
	lifetime = 80;
	rotate(angle);
	scale(1.5,1.5);

}

void missile::update()
{
	move(speed * (cos(Agnle*PI/180)), speed * (sin(Agnle*PI/180)));
	//std::cout << lifetime << std::endl;
	if(lifetime < 60)
	{
	setTextureRect(IntRect(13,0,40,16));
	}
	lifetime--;
}

missile::~missile(void)
{
}
