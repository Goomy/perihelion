#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System.hpp>
#include <random>
#include "particle.h"
using namespace sf;
class particle: public sf::Sprite
{
public:
	float life, lifetime;
	float decay;
	float Angle;
	int trans;
	Vector2f speed;
	Vector2f location;
	Vector2f gravv;
	float size;
	Texture *ptexture;
	sf::Color col;
	RectangleShape collrect;
	float xspeed, yspeed;
	particle(Vector2f initloc, double gravity, Texture *image, float lif);
	particle(Vector2f initloc, Texture *image,Color col);
	particle(){
	};
	~particle(void);
	float Randrange(float max, float min);
	void update(float y);
	Color randcol();
	Color upcol(float y);
};
