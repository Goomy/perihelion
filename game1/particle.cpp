#include "particle.h"
#include <random>
#include <cmath>
#include <iostream>

particle::particle(Vector2f initloc, double gravity, Texture *image, float lif)
{
	gravv = Vector2f(0,gravity);
	trans = 255;
	decay = 20;
	xspeed = 6.0f;
	yspeed = 8.0f;
	lifetime = lif;
	location = initloc;
	setScale(5,5);
	setColor(randcol());
	setTexture(*image);
	setTextureRect(IntRect(0,0,5,5));
	size = 10;
	speed = Vector2f((Randrange(xspeed,0)-Randrange(xspeed,0))*(Randrange(xspeed,0)-Randrange(xspeed,0)), Randrange(yspeed,2));

	setPosition(location);
}

particle::particle(Vector2f initloc, Texture *image, Color col)
{
	int sie = Randrange(5,1);
	setScale(sie,sie);
	setColor(col);
	setTexture(*image);
	setTextureRect(IntRect(0,0,5,5));
	size = 10;
	location = initloc;
	setPosition(location);
	collrect.setPosition(location);
	collrect.setSize(Vector2f(sie,sie));

}

particle::~particle(void)
{
}

void particle::update(float y)
{
	if(lifetime > 0)
	{
		lifetime -= decay;
		trans -= decay;
		if((getPosition().y-y) > 70)
		{
			size = Randrange(5,2);
			setScale(size,size);
		}
		setColor(upcol(getPosition().y-y));
		speed+=Vector2f(0,1);
		location+=speed;
		setPosition(location);
	}
}
float particle::Randrange(float max, float min)
{
	float range = (max - min);
	float rnd = min + float((range * rand()) / (RAND_MAX + 1.0));
	return rnd;
}

Color particle::randcol()
{
	switch((int) Randrange(3,0))
	{
	case 0:
		return Color(255,200,1,trans);
		break;
	case 1:
		return Color(255,100,1,trans);
		break;
	case 2:
		return Color(255,50,1,trans);
		break;
	case 3:
		return Color(0,0,255,trans);
		break;
	default:
		return Color(255,200,33,255);
		break;
	}
}

Color particle::upcol(float y)
{
	if(y < 60)
	{
		return Color(255,200,1,trans);
	}
	else if(y < 70)
	{
		return Color(255,100,1,trans);
	}
	else if( y < 80)
	{
		return Color(255,50,1,trans);
	}
	else
	{
		return Color(255,1,1,trans);
	}
}