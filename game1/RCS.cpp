#include "RCS.h"

using namespace sf;
RCS::RCS(void)
{
}


RCS::RCS(float x, float y, bool forward)
{
	va = VertexArray(Triangles,5);
	va2 = VertexArray(Points, 30);
	if(forward)
	{
		va[0].position = Vector2f(x,y);
		va[1].position = Vector2f(x+40,y-7);
		va[2].position = Vector2f(x+40, y+7);
		for(int i = 0; i < 30;i++)
		{
			va2[i].position = Vector2f((Randrange(x,0)-Randrange(x,0))*(Randrange(x,0)-Randrange(x,0)), Randrange(y,2));
			va2[i].color = Color::White;
		}
	}else{
		va[0].position = Vector2f(x,y);
		va[1].position = Vector2f(x-40,y-7);
		va[2].position = Vector2f(x-40, y+7);
	}
	va[0].color = Color::White;
	va[1].color = Color::Color(255,255,255,20);
	va[2].color = Color::Color(255,255,255,20);
}
void RCS::update(float x, float y, bool forward)
{
	if(forward)
	{
		va[0].position = Vector2f(x,y);
		va[1].position = Vector2f(x+40,y-7);
		va[2].position = Vector2f(x+40, y+7);
	}else{
		va[0].position = Vector2f(x,y);
		va[1].position = Vector2f(x-40,y-7);
		va[2].position = Vector2f(x-40, y+7);
	}

}
RCS::~RCS(void)
{
}

float RCS::Randrange(float max, float min)
{
	float range = (max - min);
	float rnd = min + float((range * rand()) / (RAND_MAX + 1.0));
	return rnd;
}
